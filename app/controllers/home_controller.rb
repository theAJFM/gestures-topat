require 'cyk_algo'
require 'input'

class HomeController < ApplicationController
  def list
    redirect_to verification_url if session[:verify] == nil
  	@grammars = Grammar.all
  	@rules = Rule.joins(:grammar)
  end
  def view
    redirect_to verification_url if session[:verify] == nil
  	@grammars = Grammar.all
  end

  def input
    redirect_to verification_url if session[:verify] == nil
  	@grammar = Grammar.new
  	@rules = Rule.new
  end

  def create
    redirect_to verification_url if session[:verify] == nil
  	@grammar = Grammar.create(name: params[:grammar][:name])
  	@counter = 0
  	data_list = params[:temp][:grammar].split("\n")
  	data_list.each do |x|
  		x.chomp!
  		Rule.create(content: x, grammar_id: @grammar.id, sequence: @counter+=1)
  	end
  	# grammar_hash = Input.parse_bypass(data_list)
  	redirect_to root_url
  end

  def check
    redirect_to verification_url if session[:verify] == nil
  	@grammar_id = params[:input][:id]
  	@string = params[:input][:string]
  	@rules = Rule.where(grammar_id: @grammar_id).order(grammar_id: :asc)
  	@data_list = []
  	@rules.each do |x| 
  		@data_list << x.content
  	end
  	@grammar_hash = Input.parse_bypass(@data_list)
  	@result = CykAlgo.cyk_algo(@string, @grammar_hash)
  	@table_result = CykAlgo.get_global_table
  	puts @table_result
  	@string_length = @string.length
  	puts @table_result
  end
  
  def verification
    redirect_to root_url if session[:verify] != nil
  end
  
  def verification_post
    @grammar_hash = Input.parse_bypass(['S -> A U', 'A -> u | d | l | r | A A', 'U -> t'])
    @result = CykAlgo.cyk_algo(params[:input][:string], @grammar_hash)
    if @result
      session[:verify] = true
      redirect_to root_url
    else
      @errors = "You've inputted the wrong gesture pattern. Try again maybe?"
      render 'verification'
    end
  end
  
  def about
    redirect_to root_url if session[:verify] == nil
  end
end
