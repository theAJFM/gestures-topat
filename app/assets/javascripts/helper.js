var ready;
var nextHide = "";
ready = function(){
    $("a").click(function(event) {
        $('#' + nextHide + 'temp').hide();
        $('#' + nextHide).attr('class', 'list-group-item');
        nextHide = event.target.id;
        $('#' + nextHide).attr('class', 'list-group-item active');
        $('#' + nextHide + 'temp').show();
    });
};
$(document).ready(ready);
$(document).on('page:load', ready);