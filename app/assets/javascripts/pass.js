var ready;
var text = "";
var stringSub = "";
ready = function(){

    var hitarea = document.getElementById("wrapper");
    var gestureControl = Hammer(hitarea);
    gestureControl.get('swipe').set({ direction: Hammer.DIRECTION_ALL });
    gestureControl.on("swipeleft", function(event) {
        text+= "arrowleft";
        stringSub += "l"
        $("#test").val("" + text);
        $("#input_string").val("" + stringSub);
    });
    gestureControl.on("swiperight", function(event) {
        text+= "arrowright";
        stringSub += "r"
        $("#test").val("" + text);
        $("#input_string").val("" + stringSub);
    });
    gestureControl.on("swipeup", function(event) {
        text+= "arrowup";
        stringSub += "u"
        $("#test").val("" + text);
        $("#input_string").val("" + stringSub);
    });
    gestureControl.on("swipedown", function(event) {
        text+= "arrowdown";
        stringSub += "d"
        $("#test").val("" + text);
        $("#input_string").val("" + stringSub);
    });
    gestureControl.on("tap", function(event) {
        text+= "circleoutline";
        stringSub += "t"
        $("#test").val("" + text);
        $("#input_string").val("" + stringSub);
    });

};
$(document).ready(ready);
$(document).on('page:load', ready);