class CreateGrammars < ActiveRecord::Migration
  def change
    create_table :grammars, id:false do |t|
      t.primary_key :ID_PASS_PATTERN
      t.string :name 
      t.timestamps
    end
  end
end
