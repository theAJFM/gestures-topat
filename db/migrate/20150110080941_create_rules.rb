class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules, id:false do |t|
      t.primary_key :ID_RULES
      t.string :content
      t.integer :grammar_id
      t.integer :sequence
      t.timestamps
    end
  end
end

