require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get check" do
    get :check
    assert_response :success
  end

  test "should get input" do
    get :input
    assert_response :success
  end

  test "should get list" do
    get :list
    assert_response :success
  end

  test "should get view" do
    get :view
    assert_response :success
  end

end
